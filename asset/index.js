
exports.parse = function (routePath)
{
  if (!routePath.assetType)
  {
    routePath.assetType = routePath.url.split(".").pop();
    switch (routePath.assetType)
    {
      case "css":
          routePath.assetType = "style";
      break;
      case "js":
          routePath.assetType = "script";
      break;
      default:
        // leave as it is
    }
  }

  return routePath;
}

exports._include = {};
exports._include['style'] = function (routePath)
{
  return `<link href="${routePath.url}" rel="stylesheet" type="text/css">`;
}
exports._include['script'] = function (routePath)
{
  return `<script src="${routePath.url}" type="text/javascript"></script>`;
}

exports.include = function (routePath)
{
  if (!routePath.assetType)
    routePath = exports.parse(routePath);

  routePath.include = exports._include[routePath.assetType](routePath);

  return routePath;
}

exports._render = {};
exports._render['style'] = function (html, includes)
{
  return html.replace(
    `</head>`,
    `${includes.join("\n")}\n</head>`
  );
}
exports._render['script'] = function (html, includes)
{
  return html.replace(
    `</body>`,
    `${includes.join("\n")}\n</body>`
  );
}

exports.render = function (html, assets)
{
  var renderedView = {};

  for (asset of assets)
  {
    asset = exports.include(asset);

    if (!renderedView[asset.assetType])
			renderedView[asset.assetType] = [];

    renderedView[asset.assetType].push(asset.include);
  }

  for (assetType in renderedView)
  {
    if (exports._render[assetType])
      html = exports._render[assetType](html, renderedView[assetType]);
  }

  return html;
}
