/*
	=================================================================================================================================================================

	HTTP Server Module

	express consolidate ejs cookie-parser body-parser multer compression cors socket.io

	* if using Passport, call it before HTTP

	Usually mapped to server.http (legacy node.httpServer)

	- Compression (GZip)
	Enabled by default

	- CORS
	https://www.npmjs.com/package/cors
	Not enabled by default
	
	httpServer.enable.cors shortcut has been deprecated
		- now CORS has to be attached to a route
		- it is recommended for this code to be placed where the routes code are
	
	Examples:
	
		To enable CORS for entire http server instance, enable it for the server base route
	
		httpServer.routes.get("base")
		.use(
			'*', 
			httpServer.m.cors(
				{
					origin: true,
					credentials: true
				}
			)
		);
		
		or use configuration preset in config
		
		httpServer.routes.get("base")
		.use(
			'*', 
			httpServer.m.cors()
		);
		
		or enable CORS on a custom route within the route mounting code
		
		route.on.mount(
			function (route, next)
			{
				httpServer.routes.get("base")
				.use(
					"/public", 
					httpServer.m.cors(),
					route.router
				);
									
				return next();
			}
		);
		
	CORS configuration
		
		origin: true is required for cross-origin to work correctly
		credentials: true is required for proper support of allowCredentials requests from clients


	=================================================================================================================================================================
*/

const {URL} = require("url");

global.n = require("ddl-node").lib;
global.node = require("ddl-node").node;

n.request = require("request");


exports.init = function (config)
{
	return new Promise(
		function(resolve, reject)
		{
			node.seq(
				{
					httpServer: {}
				},
				[
					// !---------------------------------------------
					// !initialize base and ports
					function(httpServer, next)
					{
						httpServer.config = config;
						httpServer.config.route = httpServer.config.route || httpServer.config.base || "http://localhost";
						httpServer.base = new URL(httpServer.config.route);
						
						httpServer.config.base = httpServer.base.origin;
						httpServer.config.path = httpServer.base.pathname;
						
						config = config || {};

						if(config.port && typeof(config.port)=="boolean")
						{
							var portastic = require('portastic');
							portastic.find(
							{
								min: 8000,
								max: 8039
							})
							.then(function(ports)
							{

								config.port = ports[0];
								//console.log("Server starting at : ", config.port)
								return next()
							});
						}
						else
						{
							config.port = config.port || 8080;
							//console.log("Server starting at : ", config.port)
							return next()
						}
					},

					// !---------------------------------------------
					// !initialize httpServer instance
					function(httpServer, next)
					{						
						httpServer.lib = {};
						httpServer.lib.express = require("express");

						httpServer.express = require("express")();
						httpServer.router = require("express").Router;
						httpServer.express.engine('html', require("consolidate").ejs);
						httpServer.express.set('view engine', 'html');

						httpServer.resource = {};
						httpServer.resource.cors = require('cors');
						httpServer.resource.multipart = require('multer');
						httpServer.resource.mpForm = httpServer.resource.multipart();
						httpServer.resource.helmet = require('helmet');
						httpServer.resource.resource = require('request');
						
						httpServer.express.use(require('compression')());
						httpServer.express.use(require('cookie-parser')());
						httpServer.express.use(require('body-parser').urlencoded({ extended: true, limit:'50mb' }));
						httpServer.express.use(require("body-parser").json({limit:'50mb'}));

						httpServer.asset = require('./asset');
						
						httpServer.instance = require("http").createServer(httpServer.express);

						httpServer.start = function ()
						{
							// start listening
							httpServer.instance.listen(config.port);

							console.log("HTTP service started on port "+config.port);

							return httpServer;
						}

						return next()
					},
					
					// !---------------------------------------------
					// !initialize server mounting methods
					function(httpServer, next)
					{			
						httpServer.use = function ()
						{							
							httpServer.express.use(...arguments);

							return httpServer;
						}

						httpServer._static = {};
						httpServer.static = function (routePath, dirPath, opts)
						{
							routePath = n.path.join(httpServer.base.pathname, routePath);

							httpServer._static[dirPath] = routePath;
							
							httpServer.routes.get("base").use(
								routePath,
								httpServer.lib.express.static(
									dirPath,
									opts
								)
							)

							return httpServer;
						}

						httpServer._mount = {};
						httpServer._mount['static'] = function (...args)
						{
							return new Promise(
								function (resolve, reject)
								{
									httpServer.static.apply(this, args);
									return resolve(httpServer);
								}
							);
						}

						httpServer._mount['site'] = function (site, ...args)
						{
							return site.mount(httpServer, ...args);
						}

						httpServer.mount = function (mountType, ...args)
						{
							if (httpServer._mount[mountType])
								return httpServer._mount[mountType].call(this, ...args);
						}

						/*
							res: absolute url
							= httpServer's base url + route + relative path
						*/
						httpServer.getUrlFromPath = function (targetPathFull)
						{
							var res =
							{
								path: targetPathFull,
								mountRoute: false,
								url: false
							};

							for (dirPath in httpServer._static)
							{
								//var isTargetPathPartOfDirPath = new RegExp(`^${dirPath}`, 'i');
								//if (isTargetPathPartOfDirPath.test(targetPathFull))
								
								// change from using regex to substring comparison due to regex issue with directory paths on windows
								if (targetPathFull.substring(0, dirPath.length) == dirPath)
								{
									res.mountRoute = httpServer._static[dirPath];
									res.relPath = targetPathFull.replace(dirPath, '');
									res.url = n.path.join(res.mountRoute, res.relPath);
									res.url = new URL(res.url, httpServer.base.href);
									res.url = res.url.href;
									break;
								}
							}

							return res.url;
						}									
						
						return next();
					},
					
					// !---------------------------------------------
					// !initialize server routing framework
					function(httpServer, next)
					{												
						/*
							each http instance comes with a route index
							- route modules can be created and registered onto the routes index with a route name (like ui-router route)
							- each route module has a .router and .mount method
							- route modules are loaded into this module, then a mount() function is run to mount them all
							- mounting can be called multiple times, mounted modules will be skipped
							- mounting has to be manually called instead of automatically right before starting server,
								so that the load order is maintained against other mount points outside of this routes module
						*/
						httpServer.routes = {};
						httpServer.routes.idx = {};
						httpServer.routes.arr = [];

						httpServer.routes.get = function (routeName, key='router')
						{
							var route = httpServer.routes.idx[routeName];

							if (route[key] && route[key] != httpServer.router)
								return route[key];

							return httpServer;
						}

						httpServer.routes.new = function (routeName)
						{
							var route =
							{
								name: routeName,
								_: {}
							};

							httpServer.routes.idx[routeName] = route;
							httpServer.routes.arr.push(route);

							route.router = httpServer.router;
							route.load = function (loadFn)
							{
								return new Promise(
									function (resolve, reject)
									{
										loadFn(route, resolve, reject);
									}
								)
							}

							route.on = {};

							route.on.mount = function (mountFn)
							{
								route.on._mount = mountFn;
							}

							route.mount = function ()
							{
								return new Promise(
									function (resolve, reject)
									{
										if (!route._mounted)
										{
											route._mounted = true;

											if (n.is.function(route.on._mount))
												route.on._mount(route, resolve, reject);
											else
												resolve();
										}
										else
											resolve();
									}
								);
							}

							return route;
						}

						httpServer.routes.load = function (routeModules)
						{
							return new Promise(
								function (resolve, reject)
								{
									n.async.eachOfSeries(
										routeModules,
										function (routeModule, routeModuleName, next)
										{
											//console.log("httpServer.routes.load", "route", routeModuleName);

											if (n.is.function(routeModule))
											{
												routeModule(httpServer).then(next);
											}
											else if (n.is.object(routeModule))
											{
												httpServer.routes.load(routeModule).then(next);
											}
											else
												return next();
										},
										function ()
										{
											return resolve();
										}
									)
								}
							)
						}

						httpServer.routes.mount = function ()
						{
							return new Promise(
								function (resolve, reject)
								{
									n.async.eachSeries(
										httpServer.routes.arr,
										function (route, next)
										{
											route.mount().then(next);
										},
										function ()
										{
											resolve();
										}
									)
								}
							);
						}
						
						return next();
					},
					
					// !---------------------------------------------
					// !initialize middleware helpers
					function(httpServer, next)
					{												
						httpServer.m = {};
						
						/*
							shortcut to add middleware for cors to a route
							- automatically uses config cors if available or override with custom config if provided
						*/
						httpServer.m.cors = function (conf)
						{
							if (!conf)
							{
								if (httpServer.config.cors)
									conf = httpServer.config.cors;
								else
									conf = {};
							}
							
							return httpServer.resource.cors(conf);
						}
						
						return next();
					},
					
					// !---------------------------------------------
					// !initialize base route
					function(httpServer, next)
					{												
						require("./routes")(httpServer)
						.then(
							function ()
							{										
								// mounts all routes
								httpServer.routes.mount()
								.then(
									function ()
									{							
										return next();
									}
								);
							}
						);
					}
				]
			)
			.then(
				function(httpServer)
				{
					return resolve(httpServer)
				}
			);
		}
	)

}
