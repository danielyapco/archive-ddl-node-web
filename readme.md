# dandemlabs/module/ddl-node-web
Web server module that can be required by DDL node projects to quickly startup an express server with an internal framework that uses ddl-node and supports other internal modules such as ddl-node-web-site-ng. 

### Usage
```
node.httpServer = require("ddl-node-web").init(node.conf.env.http)
```
