module.exports = function (httpServer)
{
	return httpServer.routes
	.new('base')
	.load(
		function (route, next)
		{			
			var router = route.router = route.router();
			
			router.post(
				"/info",
				function(req, res, next)
				{	
					var response = httpServer.config;
						
					res.json(response);
				}
			);
						
			route.on.mount(
				function (route, next)
				{										
					httpServer.use(httpServer.config.path, route.router);
					
					return next();
				}
			);
			
			return next();
		}
	);		
}